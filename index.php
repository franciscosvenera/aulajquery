<?
include "vars.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <nav>
        <li><a href="?page=cadAdm">Cad Adm</a></li>
        <li><a href="?page=listaAdm">Lista Adm</a></li>
        <li><a href="?page=cadCond">Cad Cond</a></li>
        <li><a href="?page=listaCond">Lista Cond</a></li>
        <li><a href="?page=cadBloco">Cad Blocos</a></li>
        <li><a href="?page=listaBloco">Lista Blocos</a></li>
    </nav>
    <div class="main">
    <?
        switch ($_GET['page']) {
        case '':
        case 'inicio':
            require "views/inicio.php";
            break;
        default:
            require 'views/'.$_GET['page'].'.php';
            break;
    }
    ?>

    </div>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
    <script src="js/<?=$_GET['page']?>.js"></script>
</body>
</html>
$(function(){

    //cadastra qualquer cadastro
    $(document).on('submit','form',function(){
        url = $(this).attr('action');
        data = $(this).serialize();
        postApi(url, data);
        return false;
    })

    //enviar post para qualquer tabela
    
    postApi = function(url,dados) {
        $.ajax({
            url: 'http://localhost/projetoFrancisco/web/index.php?r=api/default/get-token-post',
            method: 'GET',
            dataType: 'json',
            success: function (tk) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data : dados + '&_csrf='+tk.token,
                    success: function (data) {
                        if(data.endPoint.status == 'success'){
                            location.reload();
                        }
                    }
                })
            }
        })

    }

    //select administradoras
   loadAdm = function(selected){
       let idSelecionado = (selected) ? selected : null;
       options = `<option value="">Selecione...</option>`
       $.ajax({
           url: 'http://localhost/projetoFrancisco/web/index.php?r=api/administradoras/get-all',
           method: 'GET',
           dataType: 'json',
           success: function(data){
            dados = data.resultSet;   
            for(let key in dados){
                options += `<option value="${dados[key].id}"${(dados[key].id == idSelecionado ? 'selected' : '')}>${dados[key].nomeAdm}</option>`
            }
            $('.idAdmin').html(options)
           }
       })
   }
   $('.idAdmin').ready(function(){
       loadAdm();
   })

   $('.idAdmin').change(function(){
       let options = `<option value="">Selecione...</option>`
       let id = $(this).val();
       $.ajax({
           url: `http://localhost/projetoFrancisco/web/index.php?r=api/condominios/get-condominio-from-adm&idAdmin=${id}`,
           method: 'GET',
           dataType: 'json',
           success: function(data){
               let dados = data.resultSet;
               for (let key in dados) {
                   options += `<option value="${dados[key].id}">${dados[key].nomeCondominio}</option>`
               }
               $('.idCondominio').html(options)

           }
       })
   })

   $('.fromEstado').change(function(){
       let estado = $(this).val();
       let estadoSelecionado = $(this).find(`option[value="${estado}"]`).attr('data-uf');
       let options = `<option value="">Selecione...</option>`
       $.ajax({
           url: `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${estadoSelecionado}/municipios`,
           method: 'GET',
           dataType: 'json',
           success: function(data){
               for(const key in data) {
                options += `<option value="${data[key].nome}">${data[key].nome}</option>`;
               }
               $('.cidades').html(options);
           }
       })
   })

   $('.idCondominio').change(function(){
    let options = `<option value="">Selecione...</option>`
    let id = $(this).val();
    $.ajax({
        url: `http://localhost/projetoFrancisco/web/index.php?r=api/blocos/get-condominio-from-bloco&idCondominio=${id}`,
        method: 'GET',
        dataType: 'json',
        success: function(data){
            let dados = data.resultSet;
            for (let key in dados) {
                options += `<option value="${dados[key].id}">${dados[key].numeroBloco}</option>`
            }
            $('.idCondominio').html(options)

        }
    })
})
})

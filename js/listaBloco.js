$(function () {
    function getTokenApi() {
        $.ajax({
            url: 'http://localhost/projetoFrancisco/web/index.php?r=api/default/get-token-post',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                return data;
            }
        })
    }

    $('form').submit(function () {
        url = $(this).attr('action')
        data = $(this).serialize()
        postApi(url, data)
        return false
    })

    postApi = function (url, dados, reload) {
        $.ajax({
            url: 'http://localhost/projetoFrancisco/web/index.php?r=api/get-token-post',
            method: 'GET',
            dataType: 'json',
            success: function (tk) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: dados + '&_csrf=' + tk.token,
                    success: function (data) {
                       if(data.endPoint.status == 'success'){
                           location.reload()
                       }
                    }
                })
            }
        })
    }
    
    function renderizaBlocos() {
        $('#renderBlocos').html('')
        estrutura = `
            <tr>
                <td>ID</td>
                <td>Bloco</td>
                <td>Andares</td>
                <td>Unidades Andar</td>
                <td>Condominio</td>
                <td>&nbsp;</td>
            </tr>
        `
        $.ajax({
            url: 'http://localhost/projetoFrancisco/web/index.php?r=api/blocos/get-all',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.resultSet) {
                    dados = data.resultSet
                    for (let i = 0; i < data.resultSet.length; i++) {
                        estrutura += `
                        <tr data-id="${data.resultSet[i].id}">
                            <input type="hidden" name="id" value="${data.resultSet[i].id}">
                            <td>${data.resultSet[i].id}</td>
                            <td class="nomeBloco">${data.resultSet[i].nomeBloco}</td>
                            <td class="andares">${data.resultSet[i].andares}</td>
                            <td class="unidadesAndar">${data.resultSet[i].unidadesAndar}</td>
                            <td class="idCondominio">${data.resultSet[i].idCondominio}</td>
                            <td>
                                <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                                <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                            </td>
                        </tr>
                    `
                    }
                }
                $('.loader').remove()
                $('#renderBloco').html(estrutura)
            }
        })
    }
    
    renderizaBlocos()

    $(document).on('click','a[href="#"].editar', function () {
        idRegistro = $(this).attr('data-id')
        link = $(this)
        $.ajax({
            url: 'http://localhost/projetoFrancisco/web/index.php?r=api/blocos/get-one&id=' + idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                nomeBloco = data.resultSet[0].nomeBloco
                $(link).parent().parent().find('.nomeBloco').html(`<input class="form-control col-sm-6" name="nomeBloco" value="${data.resultSet[0].nomeBloco}" >`)
                andares = data.resultSet[0].andares
                $(link).parent().parent().find('.andares').html(`<input class="form-control col-sm-6" name="andares" value="${data.resultSet[0].andares}" >`)
                unidadesAndar = data.resultSet[0].unidadesAndar
                $(link).parent().parent().find('.unidadesAndar').html(`<input class="form-control col-sm-6" name="unidadesAndar" value="${data.resultSet[0].unidadesAndar}" >`)


                $(link).parent().find('.editar').toggleClass(['editar', 'salvar'])
                $(link).html('Salvar')

                $(link).parent().find('.excluir').html('Cancelar')
                $(link).parent().find('.excluir').toggleClass(['excluir', 'cancelar'])

                $('.editar, .excluir').removeAttr('href')

                
            }
        })
    })
    // CANCELA EDIÇÃO
    $(document).on('click', '.cancelar', function () {
        $(this).parent().parent().find('.nomeBloco').html(nomeBloco)
        $(this).parent().parent().find('.andares').html(andares)
        $(this).parent().parent().find('.unidadesAndar').html(unidadesAndar)

        //criar botões
        $(this).parent().find('.salvar').html('Editar')
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar'])

        $(this).parent().find('.cancelar').html('Excluir')
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'excluir'])

        $('.editar, .excluir').attr('href', '#')
    })
    
    // EDITAR REGISTRO
    $(document).on('click', '.salvar', function(){
    
        id = $(this).attr('data-id')
        numero_bloco = $('input[name="nomeBloco"]').val()
        qt_andares = $('input[name="andares"]').val()
        unidade = $('input[name="unidadesAndar"]').val()
    
        dados = `nomeBloco=${nomeBloco}&andares=${andares}&unidadesAndar=${unidadesAndar}&id=${id}`
    
        if(numero_bloco && qt_andares && unidade){
            retorno = postApi('http://localhost/projetoFrancisco/web/index.php?r=api/blocos/edit-bloco', dados)
        }
    })
    
    // DELETA REGISTRO
    $(document).on('click','.excluir', function(){
    
        idRegistro = $(this).attr('data-id')
        dados = `id=${idRegistro}`
        postApi('http://localhost/projetoFrancisco/web/index.php?r=api/blocos/delete-bloco',dados)
    })
})


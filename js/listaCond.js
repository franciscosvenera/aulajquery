$(function(){
    function renderizaCond(){
        $('#renderCond').html('');
        estrutura = `
        <tr>
            <td>ID</td>
            <td>NOME</td>
            <td>Qtde.</td>
            <td>Logradouro</td>
            <td>Número</td>
            <td>Bairro</td>
            <td>CEP</td>
            <td>Admin.</td>
            <td>Estado</td>
            <td>Cidade</td>
            <td>&nbsp;</td>
    
        </tr>
        `;
        $.ajax({
            url : 'http://localhost/projetoFrancisco/web/index.php?r=api/condominios/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
               if(data.resultSet){
                   dados = data.resultSet;
                for (let i = 0; i < data.resultSet.length; i++) {
                    estrutura += `
                    <tr data-id="${data.resultSet[i].id}">
                        <td>${data.resultSet[i].id}</td>
                        <td class="nomeCondominio">${data.resultSet[i].nomeCondominio}</td>
                        <td class="qtde">${data.resultSet[i].qtde}</td>
                        <td class="logradouro">${data.resultSet[i].logradouro}</td>
                        <td class="numero">${data.resultSet[i].numero}</td>
                        <td class="bairro">${data.resultSet[i].bairro}</td>
                        <td class="cep">${data.resultSet[i].cep}</td>
                        <td class="idAdmin">${data.resultSet[i].idAdmin}</td>
                        <td class="uf">${data.resultSet[i].uf}</td>l
                        <td class="cidade">${data.resultSet[i].cidade}</td>
                        <td>
                            <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                            <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                        </td>
                </tr>
                    `;    
                }
               }
               $('.loader').remove();
               $('#renderCond').html(estrutura);
            }
        })
    }
    renderizaCond();
    
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoFrancisco/web/index.php?r=api/condominios/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                nomeCondominio = data.resultSet[0].nomeCondominio;
                $(link).parent().parent().find('.nomeCondominio').html(`<input name="nomeCondominio" value="${data.resultSet[0].nomeCondominio}">`);
                qtde = data.resultSet[0].qtde;
                
                qtBlocos = data.resultSet[0].qtBlocos;
                $(link).parent().parent().find('.qtde').html(`<input name="qtBlocos " value="${data.resultSet[0].qtde}">`);

                idAdmin = data.resultSet[0].idAdmin;
                loadAdm(idAdmin);
                $(link).parent().parent().find('.idAdmin').html(`<select name="idAdmin" class="idAdmin"></select>`);
            }
        })
    
        //criar botões
        $(link).toggleClass(['editar', 'salvar']);
        $(link).html('Salvar');
        
        $(link).parent().find('.excluir').html('Cancelar');
        $(link).parent().find('.excluir').toggleClass(['excluir', 'cancelar']);
    
        $('.editar, .excluir').removeAttr('href');
    })
    
    //cancelar edição
    $(document).on('click','.cancelar', function(){
        $(this).parent().parent().find('.nomeCondominio').html(nomeCondominio);
        $(this).parent().parent().find('.qtde').html(qtde);
        $(this).parent().parent().find('.logradouro').html(logradouro);
        $(this).parent().parent().find('.numero').html(numero);
        $(this).parent().parent().find('.bairro').html(bairro);
        $(this).parent().parent().find('.cidade').html(cidade);
        $(this).parent().parent().find('.uf').html(uf);
        $(this).parent().parent().find('.cep').html(cep);
        $(this).parent().parent().find('.idAdmin').html(idAdmin);
    
        //criar botões
        $(this).parent().find('.salvar').html('Editar');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);
    
        $(this).parent().find('.cancelar').html('Excluir');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'excluir']);    
        
        $('.editar, .excluir').attr('href','#');
    
    })
    //editar
    $(document).on('click','.salvar',function(){
        id = $(this).attr('data-id');
        nomeCondominio = $('input[name="nomeCondominio"]').val();
        qtde = $('input[name="qtde"]').val();
        logradouro = $('input[name="logradouro"]').val();
        numero = $('input[name="numero"]').val();
        bairro = $('input[name="bairro"]').val();
        cidade = $('input[name="cidade"]').val();
        uf = $('input[name="uf"]').val();
        cep = $('input[name="cep"]').val();
        idAdmin = $('input[name="idAdmin"]').val();
    
        dados = `nomeCondominio=${nomeCondominio}&qtde=${qtde}&logradouro=${logradouro}&numero=${numero}&bairro=${bairro}&cidade=${cidade}&uf=${uf}&cep=${cep}&idAdmin=${idAdmin}&id=${id}`;
    
        if(nomeCondominio && qtde && logradouro && numero && bairro && cidade && uf && cep && idAdmin){
            retorno = postApi('http://localhost/projetoFrancisco/web/index.php?r=api/condominios/edit-cond', dados);
            
        }
    })
    
    //deletar
    $(document).on('click','.excluir', function(){
        idRegistro = $(this).attr('data-id');
        dados = `id=${idRegistro}`;
        postApi('http://localhost/projetoFrancisco/web/index.php?r=api/condominios/delete-cond', dados);
    })
    })
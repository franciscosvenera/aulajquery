$(function(){
function renderizaAdm(){
    $('#renderAdm').html('');
    estrutura = `
    <tr>
        <td>ID</td>
        <td>NOME</td>
        <td>CNPJ</td>
        <td>&nbsp;</td>

    </tr>
    `;
    $.ajax({
        url : 'http://localhost/projetoFrancisco/web/index.php?r=api/administradoras/get-all',
        method: 'GET',
        dataType: 'json',
        success: function(data){
           if(data.resultSet){
               dados = data.resultSet;
            for (let i = 0; i < data.resultSet.length; i++) {
                estrutura += `
                <tr data-id="${data.resultSet[i].id}">
                    <td>${data.resultSet[i].id}</td>
                    <td class="nomeAdm">${data.resultSet[i].nomeAdm}</td>
                    <td class="CNPJ">${data.resultSet[i].CNPJ}</td>
                    <td>
                        <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                        <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                    </td>
            </tr>
                `;    
            }
           }
           $('.loader').remove();
           $('#renderAdm').html(estrutura);
        }
    })
}
renderizaAdm();

$(document).on('click', 'a[href="#"].editar', function(){
    idRegistro = $(this).attr('data-id');
    link = $(this);
    $.ajax({
        url : 'http://localhost/projetoFrancisco/web/index.php?r=api/administradoras/get-one&id='+idRegistro,
        method: 'GET',
        dataType: 'json',
        success: function(data){
            nomeAdm = data.resultSet[0].nomeAdm;
            $(link).parent().parent().find('.nomeAdm').html(`<input name="nomeAdm" value="${data.resultSet[0].nomeAdm}">`);
            CNPJ = data.resultSet[0].CNPJ;
            $(link).parent().parent().find('.CNPJ').html(`<input name="CNPJ" value="${data.resultSet[0].CNPJ}">`);
        }
    })

    //criar botões
    $(link).toggleClass(['editar', 'salvar']);
    $(link).html('Salvar');
    
    $(link).parent().find('.excluir').html('Cancelar');
    $(link).parent().find('.excluir').toggleClass(['excluir', 'cancelar']);

    $('.editar, .excluir').removeAttr('href');
})

//cancelar edição
$(document).on('click','.cancelar', function(){
    $(this).parent().parent().find('.nomeAdm').html(nomeAdm);
    $(this).parent().parent().find('.CNPJ').html(CNPJ);

    //criar botões
    $(this).parent().find('.salvar').html('Editar');
    $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);

    $(this).parent().find('.cancelar').html('Excluir');
    $(this).parent().find('.cancelar').toggleClass(['cancelar', 'excluir']);    
    
    $('.editar, .excluir').attr('href','#');

})
//editar
$(document).on('click','.salvar',function(){
    id = $(this).attr('data-id');
    nomeAdm = $('input[name="nomeAdm"]').val();
    CNPJ = $('input[name="CNPJ"]').val();

    dados = `nomeAdm=${nomeAdm}&CNPJ=${CNPJ}&id=${id}`;

    if(nomeAdm && CNPJ){
        retorno = postApi('http://localhost/projetoFrancisco/web/index.php?r=api/administradoras/edit-adm', dados);
        
    }
})

//deletar
$(document).on('click','.excluir', function(){
    idRegistro = $(this).attr('data-id');
    dados = `id=${idRegistro}`;
    postApi('http://localhost/projetoFrancisco/web/index.php?r=api/administradoras/delete-adm', dados);
})
})
<h1>Cadastrar Condominios</h1>
<form action="http://localhost/projetoFrancisco/web/index.php?r=api/condominios/register-cond">
    
    <input class="form-control col-sm-3 ml-2" type="text" name="nomeCondominio" placeholder="Condomínio">
    <br>
    <input class="form-control col-sm-3 ml-2" type="text" name="qtde" placeholder=" Qtde. Bloco">
    <br>
    <input class="form-control col-sm-3 ml-2" type="text" name="logradouro" placeholder="Logradouro">
    <br>
    <input class="form-control col-sm-3 ml-2" type="text" name="numero" placeholder="Número">
    <br>
    <input class="form-control col-sm-3 ml-2" type="text" name="bairro" placeholder="Bairro">
    <br>
    <input class="form-control col-sm-3 ml-2" type="text" name="cep" placeholder="CEP">
    <br>
    <select name="idAdmin" class="idAdmin form-control col-sm-3 ml-2" placeholder="Administradora"></select>
    <br>
    <select name="uf" class="fromEstado form-control col-sm-3 ml-2" placeholder="Estado">
        <option value="">Selecione...</option>
        <? foreach($estadosAPI as $sig=>$uf){?>
            <option value="<?=$uf?>" data-uf="<?=$sig?>"><?=$uf?></option>
        <? } ?>
    </select>
    <br>
    <select name="cidade" class="cidades form-control col-sm-3 ml-2"></select>
    <input type="hidden" name="sindico" value="1">

    <button class="btn btn-primary mt-2 ml-2" type="submit">Enviar</button>
</form>